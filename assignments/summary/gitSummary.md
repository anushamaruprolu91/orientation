**GIT BASICS**

## what is git?

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. There are 5 advantages of Git over other source control systems are:


* Free and open source

* Distributed

* Data assurance

* Staging area

* Branching and merging


## Git Features




* ### Distributed System

Distributed systems are those which allow the users to perform work on a project from all over the world. A distributed system holds a Central repository that can be accessed by many remote collaborators by using a Version Control System.



![img](https://user-content.gitlab-static.net/1d1a983bc29f5a67cb980f55f2532a19e801b6f4/68747470733a2f2f6d656469612e6765656b73666f726765656b732e6f72672f77702d636f6e74656e742f75706c6f6164732f32303139313230333136343934382f44697374726962757465642d56657273696f6e2d436f6e74726f6c2d53797374656d2e6a7067 )

* ### Compatibility

Git is compatible with all the Operating Systems that are being used these days. Git repositories can also access the repositories of other Version Control Systems like SVN, CVK, etc.



* ### Non-linear Development**

Git allows users from all over the world to perform operations on a project remotely. A user can pick up any part of the project and do the required operation and then further update the project.


* ### Open-Source

Git is a free and open-source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. It is called open-source because it provides the flexibility to modify its source code according to the user’s needs.


## Git Terminologies




* ### Repository

Git repository is a directory that stores all the files, folders, and content needed for your project.




* ### Remote

It is a shared repository that all team members use to exchange their changes.



* ### Master

The primary branch of all repositories. All committed and accepted changes should be on the master branch.



* ### Branch

A version of the repository that diverges from the main working project or master branch.



* ### Clone

A clone is a copy of a repository or the action of copying a repository.



* ### Fetch

By performing a Git fetch, you are downloading and copying that branch’s files to your workstation.



* ### Fork

Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.



* ### Merge

Taking the changes from one branch and adding them into another (traditionally master) branch.



* ### Push

Updates a remote branch with the commits made to the current branch.



* ### Pull

If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request.

![git](https://user-content.gitlab-static.net/ad5fdb837e16fe61198879fae772377b9ec62649/68747470733a2f2f7777772e656172746864617461736369656e63652e6f72672f696d616765732f65617274682d616e616c79746963732f6769742d76657273696f6e2d636f6e74726f6c2f6769742d666f726b2d636c6f6e652d666c6f772e706e67)




## Git Workflow




* ### Working Directory

The working area is like your scratch space, it's where you can add new content, you can modify delete content, if the content that you modify or delete is in your repository you don't have to worry about losing your work.The working area, in brief, is the files that are not in the staging area.



* ### Staging Area

The staging area, that's where files are going to be a part of your next commit. It's how Git knows what is going to change between the current commit and the next one.



* ### Repository

The repository contains all of your commits. And the commit is a snapshot of what your working and staging area look like at the time of the commit. It's in your .git directory.

![Workflow](https://user-content.gitlab-static.net/4c0da883e30fe249a355bdc7a71b307dacf0038b/68747470733a2f2f692e726564642e69742f6e6d317730676e66327a6831312e706e67)



## Basic Git Commands

![Basic](https://user-content.gitlab-static.net/3b3a9edcc2be401deca54d7ac6a799cc23fe918c/68747470733a2f2f69302e77702e636f6d2f6275696c64356e696e65732e636f6d2f77702d636f6e74656e742f75706c6f6164732f323032302f30342f4769742d43686561742d53686565742d4275696c64354e696e65732e6a70673f6669743d3738372532433530362673736c3d31)
