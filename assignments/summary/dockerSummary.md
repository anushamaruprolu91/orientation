# Docker Basics
## Docker
Docker is an open source project that makes it easy to create containers and container-based apps.It is a set of platform-as-a-service products that create isolated virtualized environments for building, deploying, and testing applications.


## Docker Terminologies
* ### Docker Image
   A Docker image is an immutable (unchangeable) file that contains the source code, libraries, dependencies, tools, and other files needed for an application to run.
* ### Docker container
  A Docker container is a virtualized run-time environment where users can isolate applications from the underlying system. A container is, ultimately, just a running image. 

  ![container](https://phoenixnap.com/kb/wp-content/uploads/2019/10/container-layers.png) 
* ### Docker Hub
   Docker Hub is a SaaS repository for sharing and managing containers, where you will find official Docker images from open-source projects and software vendors and unofficial images from the general public. 
* ### Docker Files
  A Dockerfile is a simple text file that contains a list of commands the Docker client calls (on the command line) when assembling an image. 
* ### Docker Registries
  A Docker registry stores Docker images.

## Docker Architecture
Docker uses a client-server architecture. The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers. The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon. The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.

 The docker architecture is illustrated in the following figure:  

![architecture](https://geekflare.com/wp-content/uploads/2019/09/docker-architecture-609x270.png)
## Docker workflow
Step-by-step workflow for developing Docker containerized apps: 
1. Locally build and test a Docker image on your development box.
2. Build your official image for testing and deployment.
3. Deploy your Docker image to your server.
 

![img](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/docker-app-development-workflow/life-cycle-containerized-apps-docker-cli.png)

## Docker Basic Commands
* **docker ps**    :The docker ps command allows us to view all the containers that are running on the Docker Host.
* **docker start** :This command starts any stopped container(s).
* **docker stop**  :This command stops any running container(s).
* **docker run**   :This command creates containers from docker images.
* **docker rm**    :This command deletes the containers.

## Docker Common Operations
1. Downloading/pulling the docker images wanted to be worked with.
2. Copying code inside the docker
3. Accessing docker terminal
4. Installing additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try the code.

